from django.db.models import Q
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, get_object_or_404
from django.urls import reverse

from teachers.forms import TeachersCreateForm
from teachers.models import Teachers


def create_teacher(request):

    if request.method == 'GET':

        form = TeachersCreateForm()

    elif request.method == 'POST':

        form = TeachersCreateForm(request.POST)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/teachers/')

    return render(
        request=request,
        template_name='teachers-create.html',
        context={
            'form': form
        }
    )


def get_teachers(request):
    teachers = Teachers.objects.all()

    first_name = request.GET.get('first_name')
    last_name = request.GET.get('last_name')
    rating = request.GET.get('rating')

    if first_name:
        or_names = first_name.split('|')
        or_cond = Q()
        for or_name in or_names:
            or_cond = or_cond | Q(first_name=or_name)
        teachers = teachers.filter(or_cond)

    if last_name:
        teachers = teachers.filter(last_name=last_name)

    if rating:
        teachers = teachers.filter(rating=rating)

    return render(
        request=request,
        template_name='teachers-list.html',
        context={
            'teachers': teachers
        }

    )


def edit_teacher(request, id):

    try:
        teacher = Teachers.objects.get(id=id)
    except Teachers.DoesNotExist:
        return HttpResponse("Teacher doesn't exist", status=404)

    if request.method == 'GET':

        form = TeachersCreateForm(instance=teacher)

    elif request.method == 'POST':

        form = TeachersCreateForm(
            data=request.POST,
            instance=teacher
        )

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('teachers:list'))

    return render(
        request=request,
        template_name='teacher-edit.html',
        context={
            'form': form,
            'teachers': teacher
        }
    )


def delete_teacher(request, id):
    teacher = get_object_or_404(Teachers, id=id)
    teacher.delete()
    return HttpResponseRedirect(reverse('teachers:list'))
