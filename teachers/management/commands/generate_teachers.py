from django.core.management.base import BaseCommand

from teachers.models import Teachers


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('count', type=int, help=u'Количество создаваемых пользователей')

    def handle(self, *args, **kwargs):
        count = kwargs['count']
        Teachers.generate_teachers(count)
