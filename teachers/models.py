from django.db import models

from faker import Faker


class Teachers(models.Model):
    first_name = models.CharField(max_length=64, null=False)
    last_name = models.CharField(max_length=84, null=False)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

    @staticmethod
    def generate_teachers(count):
        faker = Faker()

        for _ in range(count):
            teacher = Teachers.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
            )
            teacher.save()
