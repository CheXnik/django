from django import forms

from teachers.models import Teachers


class TeachersCreateForm(forms.ModelForm):
    class Meta:
        model = Teachers
        fields = ['first_name', 'last_name']
