from django.urls import path

from teachers.views import (
    edit_teacher,
    create_teacher,
    get_teachers,
    delete_teacher,
)

app_name = 'teachers'

urlpatterns = [
    path('', get_teachers, name='list'),
    path('create/', create_teacher, name='create'),
    path('edit/<int:id>', edit_teacher, name='edit'),
    path('delete/<int:id>', delete_teacher, name='delete'),

]
