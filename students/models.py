import datetime
import random

from django.db import models

from faker import Faker

from groups.models import Group

import uuid

class Student(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    first_name = models.CharField(max_length=64, null=False)
    last_name = models.CharField(max_length=84, null=False)
    birthdate = models.DateTimeField(null=True, default=datetime.date.today)
    rating = models.SmallIntegerField(null=True, default=0)
    email = models.EmailField(null=True, max_length=128)
    phone_number = models.CharField(null=True, max_length=16)
    group = models.ForeignKey(
        to=Group,
        null=True,
        on_delete=models.SET_NULL,
        related_name='students'
    )


    def full_name(self):
        return f'{self.first_name} {self.last_name}'

    def __str__(self):
        return f'{self.id}, {self.full_name()}, {self.rating}'

    @staticmethod
    def generate_students(count):

        faker = Faker()

        for _ in range(count):
            st = Student.objects.screate(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                rating=random.randint(0, 100),
                phone_number=str(faker.phone_number()),
            )
            st.save()
