from django.conf import settings
from django.contrib import admin
from django.urls import include, path

from students.views import (
    get_random,
    hello,
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('hello/', hello),
    path('password/', get_random),

    path('', include('core.urls')),

    path('students/', include('students.urls')),

    path('teachers/', include('teachers.urls')),

    path('groups/', include('groups.urls')),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
