from django.db.models import Q
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, get_object_or_404
from django.urls import reverse

from groups.forms import GroupsCreateForm
from groups.models import Group


def get_groups(request):
    groups = Group.objects.all()

    name = request.GET.get('name')
    number_of_students = request.GET.get('rating')

    if name:
        or_names = name.split('|')
        or_cond = Q()
        for or_name in or_names:
            or_cond = or_cond | Q(name=or_name)
        groups = groups.filter(or_cond)

    if number_of_students:
        groups = groups.filter(number_of_students=number_of_students)

    return render(
        request=request,
        template_name='groups-list.html',
        context={
            'groups': groups
        }

    )


def create_group(request):

    if request.method == 'GET':

        form = GroupsCreateForm()

    elif request.method == 'POST':

        form = GroupsCreateForm(request.POST)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/groups/')

    return render(
        request=request,
        template_name='Groups-create.html',
        context={
            'form': form
        }
    )


def edit_group(request, id):

    try:
        group = Group.objects.get(id=id)
    except Group.DoesNotExist:
        return HttpResponse("Group doesn't exist", status=404)

    if request.method == 'GET':

        form = GroupsCreateForm(instance=group)

    elif request.method == 'POST':

        form = GroupsCreateForm(
            data=request.POST,
            instance=group
        )

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('groups:list'))

    return render(
        request=request,
        template_name='groups-edit.html',
        context={
            'form': form,
            'groups': group
        }
    )


def delete_group(request, id):
    group = get_object_or_404(Group, id=id)
    group.delete()
    return HttpResponseRedirect(reverse('groups:list'))
