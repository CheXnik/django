from django.urls import path

from groups.views import (
    edit_group,
    create_group,
    get_groups,
    delete_group,
)

app_name = 'groups'

urlpatterns = [
    path('', get_groups, name='list'),
    path('create/', create_group, name='create'),
    path('edit/<int:id>', edit_group, name='edit'),
    path('delete/<int:id>', delete_group, name='delete'),
]
