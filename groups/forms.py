from django import forms

from groups.models import Group


class GroupsCreateForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = ['name', 'number_of_students']
