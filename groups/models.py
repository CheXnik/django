from django.db import models

from faker import Faker


class Group(models.Model):
    name = models.CharField(max_length=64, null=False)
    number_of_students = models.SmallIntegerField(max_length=64, null=False)


    def __str__(self):
        return f'{self.name}, {self.number_of_students}'

    @staticmethod
    def generate_group(count):
        faker = Faker()

        for _ in range(count):
            group = Group.objects.create(
                name=faker.first_name(),
            )
            group.save()
